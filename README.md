# gitlab-list-of-projects

Choose a self-explaining name for your project.

## Description


This script with generate a csv list of projects in the GROUP_PATH, including the subgroups, you can access.
It uses pagination to extract the full list.

Extracts the following data as CSV output:
`Project Name, Project Path, Visibility, Archived, Creation Date`

## Installation
download the script localy and run from cmd-line

## Usage

```
# Replace ${GITLAB_PRIVATE_TOKEN} with your GitLab access token or `export GITLAB_PRIVATE_TOKEN="your token"` before running
# Replace ${GROUP_PATH} with the path of the top-level group (e.g., "your-group" or "your-group/sub-group") or `export GROUP_PATH="path"` before running
```

## Contributing
feel free to open MR to this project if needed.

## Authors and acknowledgment
@comeh, 2023

## License
MIT

## Project status
it worked when I needed it, no supported but feel free to use it :-) 
