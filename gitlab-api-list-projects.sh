#!/bin/bash 

# Replace ${GITLAB_PRIVATE_TOKEN} with your GitLab access token or `export GITLAB_PRIVATE_TOKEN="your token"` before running
# Replace ${GROUP_PATH} with the path of the top-level group (e.g., "your-group" or "your-group/sub-group") or `export GROUP_PATH="path"` before running

base_url="https://gitlab.com/api/v4/groups/${GROUP_PATH}/projects"
page=1
per_page=100

echo "Project Name, Project Path, Visibility, Archived, Creation Date"

while true; do
    response=$(curl -s --header "PRIVATE-TOKEN: ${GITLAB_PRIVATE_TOKEN}" "${base_url}?include_subgroups=true&page=${page}&per_page=${per_page}")
    projects=$(echo "$response" | jq -r '.[] | [.name, .path_with_namespace, .visibility, .archived, .created_at] | @tsv')

    if [ -z "$projects" ]; then
        # No more projects, break out of the loop
        break
    fi

    # Fetch user details for each project
    while IFS=$'\t' read -r name path visibility archived created_at created_by; do
        user_response=$(curl -s --header "PRIVATE-TOKEN: <YOUR_PRIVATE_TOKEN>" "https://gitlab.com/api/v4/users/${created_by}")
        user_name=$(echo "$user_response" | jq -r '.name // empty')

	echo "$name, $path, $visibility, $archived, $created_at"
    done <<< "$projects"

    page=$((page + 1))
done
